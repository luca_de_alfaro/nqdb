# Luca de Alfaro, 2020
# BSD License

import json

from nqdb import Model, Property, StringProperty, DateTimeProperty
from nqdb import JsonProperty, BlobProperty, IntegerProperty, FloatProperty
from nqdb import BooleanProperty, TextProperty
from nqdb import DatastoreClient
from nqdb import put, get, delete, Key
from nqdb import TestCache

from tester import do, dont, do_all_tests

MOCK = False

if not MOCK:
    # We use a real datastore with test keys.
    import os
    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = "keys/test_keys.json"

cache = TestCache()
client = DatastoreClient(cache=cache, mock=MOCK)

class Person(Model):
    _client = client
    first_name = StringProperty('first_name')
    last_name = StringProperty('last_name')
    last_seen = DateTimeProperty('last_seen', auto_now=True)
    when = DateTimeProperty('when')
    jsonprop1 = JsonProperty('jsonprop1')
    jsonprop2 = JsonProperty('jsonprop2', compressed=False)
    blob1 = BlobProperty('blob1')
    blob2 = BlobProperty('blob2', compressed=True)
    age = IntegerProperty('age')
    height = FloatProperty('height')
    minor = BooleanProperty('minor')
    intro = TextProperty('intro')
    abstract = TextProperty('absract', compressed=True)
    date_created = DateTimeProperty('date_created', auto_now_add=True)
    date_updated = DateTimeProperty('date_updated', auto_now=True)

@do
def test_put_get():
    p = Person()
    p.first_name = "Luca"
    p.last_name = "de Alfaro"
    age = 26
    height = 1.66
    minor = False
    intro = "This is me"
    abstract = "I love to cook for the family"
    j1 = {'idnumber': 746843234}
    j2 = {'idnumber': 846843234}
    p.jsonprop1 = j1
    p.jsonprop2 = j2
    b1 = b"blob1"
    b2 = b"blob2"
    p.blob1 = b1
    p.blob2 = b2
    p.age = age
    p.height = height
    p.minor = minor
    p.intro = intro
    p.abstract = abstract
    p.put()
    id = p.key_id
    print("id:", id)
    q = Person(id=id)
    q.get()
    print("First name:", q.first_name)
    print("Last name:", q.last_name)
    print("Last seen:", q.last_seen)
    print("Jsonprop1:", q.jsonprop2)
    print("Jsonprop2:", q.jsonprop1)
    print("blob1:", q.blob1)
    print("blob2:", q.blob2)
    assert q.blob1 == b1
    assert q.blob2 == b2
    assert q.jsonprop1 == j1
    assert q.jsonprop2 == j2
    assert q.age == age, "%r %r" % (q.age, age)
    assert q.height == height
    assert q.minor == minor
    assert q.intro == intro, "%r %r" % (q.intro, intro)
    assert q.abstract == abstract


@do
def test_put_get_null():
    p = Person()
    p.first_name = "Luca"
    p.last_name = None
    age = None
    height = None
    minor = None
    intro = "This is me"
    abstract = "I love to cook for the family"
    j1 = {'idnumber': 746843234}
    j2 = {'idnumber': 846843234}
    p.jsonprop1 = j1
    p.jsonprop2 = j2
    b1 = b"blob1"
    b2 = b"blob2"
    p.blob1 = b1
    p.blob2 = b2
    p.age = age
    p.height = height
    p.minor = minor
    p.intro = intro
    p.abstract = abstract
    p.put()
    id = p.key_id
    print("id:", id)
    q = Person(id=id)
    q.get()
    print("First name:", q.first_name)
    print("Last name:", q.last_name)
    print("Last seen:", q.last_seen)
    print("Jsonprop1:", q.jsonprop2)
    print("Jsonprop2:", q.jsonprop1)
    print("blob1:", q.blob1)
    print("blob2:", q.blob2)
    assert q.blob1 == b1
    assert q.blob2 == b2
    assert q.jsonprop1 == j1
    assert q.jsonprop2 == j2
    assert q.age == age
    assert q.height == height
    assert q.minor == minor
    assert q.intro == intro
    assert q.abstract == abstract



@do
def test_key_id():
    p = Person()
    p.first_name = "Joe"
    p.last_name = "Falchetto"
    put(p)
    id = p.key_id
    print("Id:", repr(id))
    assert id is not None


@do
def test_nqdb_put_get():
    p = Person()
    p.first_name = "Joe"
    p.last_name = "Falchetto"
    put(p)
    id = p.key_id
    print("Id:", repr(id))
    k = Key(Person, id)
    print("Created a key:", k)
    print("Key kind:", k.model_class._kind)
    q = get(k)
    assert q.last_name == p.last_name
    assert q.first_name == p.first_name
    assert q.key_id == p.key_id


@do
def test_nqdb_key_put_get():
    p = Person()
    p.first_name = "Joe"
    p.last_name = "Falchetto"
    put(p)
    id = p.key_id
    print("Id:", repr(id))
    k = Key(Person, id)
    print("Created a key:", k)
    print("Key kind:", k.model_class._kind)
    q = k.get()
    assert q.last_name == p.last_name
    assert q.first_name == p.first_name
    assert q.key_id == p.key_id


@do
def test_delete():
    cache.clear()
    p = Person()
    p.first_name = "Joe"
    p.last_name = "Falchetto"
    put(p)
    id = p.key_id
    print("Id:", repr(id))
    k = Key(Person, id)
    print("Key to delete:", k)
    delete(k)
    print("Cache:", cache)
    q = get(k)
    assert q is None

@do
def test_nones():
    p = Person()
    put(p)
    id = p.key_id
    k = Key(Person, id)
    q = get(k)
    assert q.first_name == None
    assert q.last_name == None
    assert q.jsonprop1 == None
    assert q.jsonprop2 == None
    assert q.blob1 == None
    assert q.blob2 == None

@do
def test_nones_set():
    p = Person()
    p.first_name = None
    p.last_name = None
    p.when = None
    p.jsonprop1 = None
    p.jsonprop2 = None
    p.blob1 = None
    p.blob2 = None
    put(p)
    id = p.key_id
    k = Key(Person, id)
    q = get(k)
    assert q.first_name == None
    assert q.last_name == None
    assert q.when == None
    assert q.jsonprop1 == None
    assert q.jsonprop2 == None
    assert q.blob1 == None
    assert q.blob2 == None

do_all_tests()

