# Luca de Alfaro, 2020
# BSD License


from nqdb import Model, DatastoreClient
from nqdb.property import *

from tester import do, dont, do_all_tests

MOCK = False

if not MOCK:
    # We use a real datastore with test keys.
    import os
    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = "keys/test_keys.json"

client = DatastoreClient(mock=MOCK)

class Person(Model):
    _client = client
    first_name = StringProperty('first_name')
    last_name = TextProperty('last_name', compressed=False)
    address = TextProperty('address', compressed=True)
    info = JsonProperty('info', compressed=False)
    extra = JsonProperty('extra', compressed=True)

S1 = "The cat is out of the bag"
S2 = "Pot watched never boils"
J1 = {"cost": 0.3, "notes": "Not to be set on fire"}
J2 = {"price": None, "info": [1, 3, 4, 8]}

@do
def test_text_property():
    p = Person()
    p.first_name = "Luca"
    p.last_name = S1
    p.address = S2
    p.info = J1
    p.extra = J2
    p.put()

    assert p.last_name == S1
    assert p.address == S2
    assert p.info == J1
    assert p.extra == J2

    id = p.key_id
    print("id:", id)
    q = Person(id=id)
    q.get()

    assert q.last_name == S1
    assert q.address == S2
    assert q.info == J1
    assert q.extra == J2

class Event(Model):
    _client = client
    name = StringProperty('name')
    info = StringProperty('info')
    date_created = DateTimeProperty('date_created', auto_now_add=True)
    date_updated = DateTimeProperty('date_updated', auto_now=True)

@do
def test_date_property():
    e = Event()
    first_info = "First information"
    second_info = "Second information"
    date_created = e.date_created
    e.info = first_info
    print("Before writing")
    print("Created:", e.date_created)
    print("Updated:", e.date_updated)
    e.put()
    id = e.key_id
    q = Event(id=id)
    q.get()
    assert q.info == first_info, q.info
    date_updated_1 = q.date_updated
    print("date_updated_1:", date_updated_1)
    print("date_created:", date_created)
    assert date_updated_1 > date_created
    assert q.date_created == date_created
    print("First time read")
    print("Created:", q.date_created)
    print("Updated:", q.date_updated)
    print("Writing e again")
    e.info = second_info
    e.put()
    print("Second time read")
    # q = Event(id=id) # Then remove.
    q.get()
    assert q.info == second_info, q.info
    date_updated_2 = q.date_updated
    print("date_updated_1", date_updated_1)
    print("date_updated_2", date_updated_2)
    assert date_updated_2 > date_updated_1
    assert q.date_created == date_created
    print("Created:", q.date_created)
    print("Updated:", q.date_updated)
    # Can we also write it?
    q.put()

@do
def test_initialization():
    p = Person(first_name="Luca", last_name="de Alfaro")
    assert p.first_name == "Luca"
    assert p.last_name == "de Alfaro"

@do
def test_date_defaults():
    ev = Event()
    d0 = datetime.datetime.now(datetime.timezone.utc)
    d1 = datetime.datetime.now(datetime.timezone.utc) - datetime.timedelta(hours=1)
    print("d0:", d0)
    print("d1:", d1)
    ev.date_created = d0
    ev.date_updated = d1
    ev.put()
    q = Event(id=ev.key_id)
    q.get()
    assert q.date_created == d0, q.date_created
    assert q.date_updated >= d0


do_all_tests()

