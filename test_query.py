# Luca de Alfaro, 2020
# BSD License


from nqdb import Model, Property, StringProperty, IntegerProperty, FloatProperty
from nqdb import DatastoreClient
from nqdb import put, put_multi, get, delete, Key

from tester import do, dont, do_all_tests

MOCK = False

if not MOCK:
    # We use a real datastore with test keys.
    import os
    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = "keys/test_keys.json"

client = DatastoreClient(mock=MOCK)

class TestEntity(Model):
    _client = client
    name = StringProperty('name')
    ordinal = IntegerProperty('ordinal')
    flo = FloatProperty('flo')

def setup():
    for i in range(10):
        print("Creating TestEntity", i)
        e = TestEntity(id=i + 1) # An id of 0 is invalid, not sure why.
        e.name = "Entity_%d" % i
        e.ordinal = i
        e.flo = (10 - i) / 2
        e.put()

@dont
def test_insert():
    # We need to do this only once.  No harm if it is done more than once though.
    setup()

@do
def test_int_query():
    # Tests a simple query on integers.
    print("Reserved:", Property.__reserved__)
    q = TestEntity.query()
    q.filter(TestEntity.ordinal > 4)
    q.order(+TestEntity.ordinal)
    for e in q.fetch():
        print ("Retrieved", e.name, "with ordinal", e.ordinal)

do_all_tests()

