from nqdb import Model, DatastoreClient, TestCache
from nqdb.property import *

from tester import do, dont, do_all_tests

MOCK = False

if not MOCK:
    # We use a real datastore with test keys.
    import os
    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = "keys/test_keys.json"

client = DatastoreClient(mock=MOCK)

class Person(Model):
    _client = client
    first_name = StringProperty('first_name')
    nicknames = StringProperty('nicknames', repeated=True)
    codes = StringProperty('codes', repeated=True)
    extra = StringProperty('extra', repeated=True)

@do
def test_repeated():
    p = Person()
    p.first_name = "Luca"
    p.nicknames = ["Peter", "Pan"]
    p.codes = []
    assert p.nicknames == ["Peter", "Pan"]
    assert p.codes == []
    assert p.extra == None

    p.put()
    id = p.key_id
    print("id:", id)
    q = Person(id=id)
    q.get()

    assert q.nicknames == ["Peter", "Pan"]
    assert q.codes == []
    assert q.extra == None

# Now testing with cache.
cache = TestCache()
caching_client = DatastoreClient(cache=cache, mock=True)

class UncachedPerson(Model):
    _client = client
    first_name = StringProperty('first_name')
    nicknames = StringProperty('nicknames', repeated=True)
    vacations = DateTimeProperty('vacations', repeated=True)
    numbers = IntegerProperty('numbers', repeated=True)
    floats = FloatProperty('flotas', repeated=True)
    booleans = BooleanProperty('booleans', repeated=True)

class CachedPerson(Model):
    _client = caching_client
    first_name = StringProperty('first_name')
    nicknames = StringProperty('nicknames', repeated=True)
    vacations = DateTimeProperty('vacations', repeated=True)
    numbers = IntegerProperty('numbers', repeated=True)
    floats = FloatProperty('flotas', repeated=True)
    booleans = BooleanProperty('booleans', repeated=True)

@do
def test_repeated_uncached():
    p = UncachedPerson()
    p.first_name = "Luca"
    nicknames = ["Peter", "Pan"]
    d1 = datetime.datetime.now(datetime.timezone.utc)
    d2 = d1 + datetime.timedelta(days=2)
    vacations = [d1, d2]
    numbers = [1, 2, 3]
    floats = [3.14, 2.718]
    booleans = [True, False]
    p.nicknames = nicknames
    p.vacations = vacations
    p.numbers = numbers
    p.floats = floats
    p.booleans = booleans
    assert p.nicknames == nicknames
    assert p.vacations == vacations
    assert p.numbers == numbers
    assert p.floats == floats
    assert p.booleans == booleans
    p.put()
    id = p.key_id
    q = UncachedPerson(id=id)
    q.get()
    assert q.nicknames == nicknames
    assert q.vacations == vacations
    assert q.numbers == numbers
    assert q.floats == floats
    assert q.booleans == booleans

@do
def test_repeated_uncached():
    p = CachedPerson()
    p.first_name = "Luca"
    nicknames = ["Peter", "Pan"]
    d1 = datetime.datetime.now(datetime.timezone.utc)
    d2 = d1 + datetime.timedelta(days=2)
    vacations = [d1, d2]
    numbers = [1, 2, 3]
    floats = [3.14, 2.718]
    booleans = [True, False]
    p.nicknames = nicknames
    p.vacations = vacations
    p.numbers = numbers
    p.floats = floats
    p.booleans = booleans
    assert p.nicknames == nicknames
    assert p.vacations == vacations
    assert p.numbers == numbers
    assert p.floats == floats
    assert p.booleans == booleans
    p.put()
    id = p.key_id
    q = CachedPerson(id=id)
    q.get()
    assert q.nicknames == nicknames
    assert q.vacations == vacations
    assert q.numbers == numbers
    assert q.floats == floats
    assert q.booleans == booleans


do_all_tests()