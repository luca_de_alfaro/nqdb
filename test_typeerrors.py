# Luca de Alfaro, 2020
# BSD License


from nqdb import Model, DatastoreClient
from nqdb.property import *
from nqdb import TestCache

from tester import do, dont, do_all_tests

MOCK = True

if not MOCK:
    # We use a real datastore with test keys.
    import os
    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = "keys/test_keys.json"

cache = TestCache()
client = DatastoreClient(cache=cache, mock=MOCK)

class IntModel(Model):
    _client = client
    my_int = IntegerProperty('my_int')
    my_int_rep = IntegerProperty('my_int', repeated=True)
    my_int_req = IntegerProperty('my_int_req', required=True)

@do
def test_int_ok():
    p = IntModel()
    p.my_int = 2
    p.my_int_rep = 3
    p.my_int_req = 4
    assert p.my_int_rep == [3]

@do
def test_int_missing():
    p = IntModel()
    p.my_int = 2
    p.my_int_rep = 3
    try:
        p.put()
        assert False
    except NQDBTypeError as e:
        print("ok:", e)

@do
def test_int_rep():
    p = IntModel()
    try:
        p.my_int = 2.3
        assert False
    except NQDBTypeError as e:
        print("ok:", e)

do_all_tests()

