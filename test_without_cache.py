# Luca de Alfaro, 2020
# BSD License


from nqdb import Model, Property, StringProperty, DatastoreClient
from nqdb import put, put_multi, get, delete, Key

from tester import do, dont, do_all_tests

MOCK = False

if not MOCK:
    # We use a real datastore with test keys.
    import os
    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = "keys/test_keys.json"

client = DatastoreClient(mock=MOCK)

class Person(Model):
    _client = client
    first_name = StringProperty('first_name')
    last_name = StringProperty('last_name')

@do
def test_put_get():
    p = Person()
    p.first_name = "Luca"
    p.last_name = "de Alfaro"
    p.put()
    id = p.key_id
    print("id:", id)
    q = Person(id=id)
    q.get()
    print("First name:", q.first_name)
    print("Last name:", q.last_name)

@do
def test_key_id():
    p = Person()
    p.first_name = "Joe"
    p.last_name = "Falchetto"
    put(p)
    id = p.key_id
    print("Id:", repr(id))
    assert id is not None


@do
def test_nqdb_put_get():
    p = Person()
    p.first_name = "Joe"
    p.last_name = "Falchetto"


@do
def test_nqdb_put_get():
    p = Person()
    p.first_name = "Joe"
    p.last_name = "Falchetto"
    put(p)
    id = p.key_id
    print("Id:", repr(id))
    k = Key(Person, id)
    print("Created a key:", k)
    print("Key kind:", k.model_class._kind)
    q = get(k)
    assert q.last_name == p.last_name
    assert q.first_name == p.first_name
    assert q.key_id == p.key_id

@do
def test_nqdb_put_get():
    p = Person()
    p.first_name = "Joe"
    p.last_name = "Falchetto"
    q = Person()
    q.first_name = "Luca"
    q.last_name = "de Alfaro"
    put_multi([p, q])


@do
def test_key_get():
    p = Person()
    p.first_name = "Joe"
    p.last_name = "Falchetto"
    put(p)
    id = p.key_id
    print("Id:", repr(id))
    k = Key(Person, id)
    print("Created a key:", k)
    print("Key kind:", k.model_class._kind)
    q = k.get()
    assert q.last_name == p.last_name
    assert q.first_name == p.first_name
    assert q.key_id == p.key_id


@do
def test_delete():
    p = Person()
    p.first_name = "Joe"
    p.last_name = "Falchetto"
    put(p)
    id = p.key_id
    print("Id:", repr(id))
    k = Key(Person, id)
    delete(k)
    q = get(k)
    assert q is None


do_all_tests()

